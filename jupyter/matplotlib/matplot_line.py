#!/usr/bin/env python3

# https://matplotlib.org/examples/pylab_examples/simple_plot.html
# PY2:
#   apt-get install python-matplotlib
#   o
#   python -m pip install -U pip
#   python -m pip install -U matplotlib

# PY3: apt-get install python3-matplotlib
#   o
#   python -m pip3 install -U pip
#   python -m pip3 install -U matplotlib




import matplotlib.pyplot as plt
plt.plot([1,2,3,4])
plt.ylabel('some numbers')
plt.show()

