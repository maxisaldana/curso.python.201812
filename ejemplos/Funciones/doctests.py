def promedio(valores):
    """Calcula la media aritmética de una lista de números.

    >>> print(promedio([20, 30, 70]))
    40.0

    >>> print(promedio([1]))
    1.0

    >>> print(promedio([1,2]))
    1.1

    """
    return sum(valores) / len(valores)

def suma(valores):
    '''Suma la lista de "valores"

    >>> suma((1,2))
    1
    '''
    #TODO: implementar!!
    pass

if __name__=='__main__':

    print("MAIN")
    import doctest

    doctest.testmod()
    # valida automáticamente las pruebas integradas
