#!/usr/bin/env python2

# https://matplotlib.org/examples/pylab_examples/simple_plot.html
# PY2:
#   apt-get install python-matplotlib
#   o
#   python -m pip install -U pip
#   python -m pip install -U matplotlib

# PY3: apt-get install python3-matplotlib
#   o
#   python -m pip3 install -U pip
#   python -m pip3 install -U matplotlib




import matplotlib.pyplot as plt
import numpy as np

t = np.arange(0.0, 2.0, 0.01)
s = 1 + np.sin(2*np.pi*t)
plt.plot(t, s)

plt.xlabel('time (s)')
plt.ylabel('voltage (mV)')
plt.title('About as simple as it gets, folks')
plt.grid(True)
plt.savefig("test.png")
plt.show()

