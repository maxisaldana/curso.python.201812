

# https://en.wikipedia.org/wiki/Trial_division

An example of the trial division algorithm, using successive integers as trial factors, is as follows (in Python):

 1 def trial_division(n):
 2 	"""Return a list of the prime factors for a natural number."""
 3 	a = []               #Prepare an empty list.
 4 	f = 2                #The first possible factor.	
 5 	while n > 1:         #While n still has remaining factors...
 6 		if n % f == 0:     #The remainder of n divided by f might be zero.        
 7 			a.append(f)         #If so, it divides n. Add f to the list.
 8 			n /= f              #Divide that factor out of n.
 9 		else:                #But if f is not a factor of n,
10 			f += 1              #Add one to f and try again.
11 	return a             #Prime factors may be repeated: 12 factors to 2,2,3.
Or 2x more efficient:

 1 def trial_division(n):
 2     a = []  
 3     while n % 2 == 0:
 4         a.append(2)
 5         n /= 2
 6     f = 3
 7     while f * f <= n:
 8         if n % f == 0:
 9             a.append(f)
10             n /= f
11         else:
12             f += 2   
13     if n != 1: a.append(n)
14     #Only odd number is possible
15     return a


