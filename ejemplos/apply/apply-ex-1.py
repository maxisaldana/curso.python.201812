#!/usr/bin/env python

# Para visualizar:
#  Ejecutar en:
#   ipython o jupyter notebook

# Astronomical Plotting Library

# https://aplpy.readthedocs.io/en/stable/
# https://aplpy.readthedocs.io/en/stable/fitsfigure/quickstart.html
# https://media.readthedocs.org/pdf/aplpy/latest/aplpy.pdf

# pip install aplpy
# pip3 install aplpy

# ipython --pylab


#import matplotlib
#import numpy

import aplpy

# gc = aplpy.FITSFigure('fits/2MASS_k.fits')
gc = aplpy.FITSFigure('tutorial/tutorial/fits/2MASS_k.fits')

gc.show_grayscale()

gc.show_colorscale()

gc.show_colorscale(cmap='gist_heat')

gc.show_rgb('tutorial/tutorial/graphics/2MASS_arcsinh_color.png')

import numpy
data = numpy.loadtxt('tutorial/tutorial/data/yso_wcs_only.txt')
ra, dec = data[:, 0], data[:, 1]
gc.show_markers(ra, dec, 
                edgecolor='green', 
                facecolor='none',
                marker='o', 
                s=10, 
                alpha=0.5,
)

