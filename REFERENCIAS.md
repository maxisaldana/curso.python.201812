

GitLab Neoris-training
https://gitlab.com/neoris-training/training-python-spanish-2018






https://www.python-course.eu/python3_execute_script.php

https://realpython.com/python-modules-packages/#the-dir-function

https://stackoverflow.com/questions/16997950/whats-the-difference-between-module-package-and-library-in-haskell

https://stackoverflow.com/questions/19198166/whats-the-difference-between-a-module-and-a-library-in-python


--------------
Only package and module have a well-defined meaning specific to Python.

An API is not a collection of code per se - it is more like a "protocol" specification how various parts (usually libraries) communicate with each other. There are a few notable "standard" APIs in python. E.g. the DB API

In my opinion, a library is anything that is not an application - in python, a library is a module - usually with submodules. The scope of a library is quite variable - for example the python standard library is vast (with quite a few submodules) while there are lots of single purpose libraries in the PyPi, e.g. a backport of collections.OrderedDict for py < 2.7

A package is a collection of python modules under a common namespace. In practice one is created by placing multiple python modules in a directory with a special __init__.py module (file).

A module is a single file of python code that is meant to be imported. This is a bit of a simplification since in practice quite a few modules detect when they are run as script and do something special in that case.

A script is a single file of python code that is meant to be executed as the 'main' program.

If you have a set of code that spans multiple files, you probably have an application instead of script.


--------------













