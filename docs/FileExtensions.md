

.py 	source code
.pyc	compiled bytecode
.pyo	pre Python 3.5 : bytecode "optimized" (-O)
		- assert
.pyd	basically a windows dll file

----- ----- ----- ----- ----- ----- ----- ----- ----- ----- 

Otros:

.py - Regular script
.py3 - (rarely used) Python3 script. Python3 scripts usually end with ".py" not ".py3", but I have seen that a few times
.pyc - compiled script (Bytecode)
.pyo - optimized pyc file (As of Python3.5, Python will only use pyc rather than pyo and pyc)
.pyw - Python script to run in Windowed mode, without a console; executed with pythonw.exe
.pyx - Cython src to be converted to C/C++
.pyd - Python script made as a Windows DLL
.pxd - Cython script which is equivalent to a C/C++ header
.pxi - MyPy stub
.pyi - Stub file (PEP 484)
.pyz - Python script archive (PEP 441); this is a script containing compressed Python scripts (ZIP) in binary form after the standard Python script header
.pywz - Python script archive for MS-Windows (PEP 441); this is a script containing compressed Python scripts (ZIP) in binary form after the standard Python script header
.py[cod] - wildcard notation in ".gitignore" that means the file may be ".pyc", ".pyo", or ".pyd".
A larger list of additional Python file-extensions (mostly rare and unofficial) can be found at http://dcjtech.info/topic/python-file-extensions/



----- ----- ----- ----- ----- ----- ----- ----- ----- ----- 
http://docs.python.org/faq/windows.html#is-a-pyd-file-the-same-as-a-dll


----- ----- ----- ----- ----- ----- ----- ----- ----- ----- 
http://www.network-theory.co.uk/docs/pytut/CompiledPythonfiles.html


When the Python interpreter is invoked with the -O flag, optimized code is generated and stored in ‘.pyo’ files. The optimizer currently doesn't help much; it only removes assert statements. When -O is used, all bytecode is optimized; .pyc files are ignored and .py files are compiled to optimized bytecode.
Passing two -O flags to the Python interpreter (-OO) will cause the bytecode compiler to perform optimizations that could in some rare cases result in malfunctioning programs. Currently only __doc__ strings are removed from the bytecode, resulting in more compact ‘.pyo’ files. Since some programs may rely on having these available, you should only use this option if you know what you're doing.
A program doesn't run any faster when it is read from a ‘.pyc’ or ‘.pyo’ file than when it is read from a ‘.py’ file; the only thing that's faster about ‘.pyc’ or ‘.pyo’ files is the speed with which they are loaded.
When a script is run by giving its name on the command line, the bytecode for the script is never written to a ‘.pyc’ or ‘.pyo’ file. Thus, the startup time of a script may be reduced by moving most of its code to a module and having a small bootstrap script that imports that module. It is also possible to name a ‘.pyc’ or ‘.pyo’ file directly on the command line.

----- ----- ----- ----- ----- ----- ----- ----- ----- ----- 
