EJERCICIO SUGERIDO

Aplicación en línea de comando.

Funcionalidad:
 + capaz de acceder a una hoja de cálculo en línea, preexistente dentro de GoogleSheets
 + genera diccionario (unicidad)
 + puede leer el contenido de ciertas columnas de una hoja específica
 + puede introducir valores en una hoja
 
 Adicionalmente:
  - parametrizar:
    - id de la hoja
    - credenciales de acceso
    - detino de la inserción
    - tipo de elaboración de la data a insertar
  - la salida puede indicarse del mismo modo
  - puede programarse en una función, OOP, FP
  - puede implementarse en módulos

Versión 0:
 - entrada: Hardcoded
 - salida: StdOut
 
 Versión 1:
 - entrada: StdIn 
 - salida: StdOut
 
 
Sugerencias:
 - Realizarlo en varias iteraciones
 - Documentar funcionalidades
 - Generar tests:
    - doctest
    - pytest
    
    