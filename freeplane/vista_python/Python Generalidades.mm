<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Python&#xa;Generalidades" FOLDED="false" ID="ID_191153586" CREATED="1551356995725" MODIFIED="1551402452111" ICON_SIZE="36.0 pt" STYLE="oval">
<font SIZE="22"/>
<hook NAME="MapStyle" zoom="1.1">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" ICON_SIZE="64.0 pt" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font NAME="Segoe Print" SIZE="22"/>
<edge COLOR="#ffffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" ICON_SIZE="32.0 px" COLOR="#000000" BACKGROUND_COLOR="#ffffcc" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="2.6 pt" SHAPE_VERTICAL_MARGIN="2.6 pt" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="18" BOLD="false" ITALIC="true"/>
<edge STYLE="sharp_bezier" WIDTH="8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" ICON_SIZE="28.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="16"/>
<edge STYLE="bezier" WIDTH="4"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" ICON_SIZE="24.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="14"/>
<edge STYLE="bezier" WIDTH="3"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" ICON_SIZE="24.0 px" COLOR="#111111" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="13"/>
<edge STYLE="bezier" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" ICON_SIZE="24.0 px" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="12"/>
<edge STYLE="bezier" WIDTH="1"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" ICON_SIZE="24.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" ICON_SIZE="16.0 px">
<font SIZE="10"/>
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="17" RULE="ON_BRANCH_CREATION"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<edge COLOR="#ffffff"/>
<hook URI="python-logo-generic-11-2.png" SIZE="1.0" NAME="ExternalObject"/>
<node TEXT="Encoding" POSITION="right" ID="ID_495676614" CREATED="1551356995732" MODIFIED="1551402533217" HGAP_QUANTITY="101.80488213352491 pt" VSHIFT_QUANTITY="-42.14634342409195 pt">
<edge COLOR="#009900"/>
<node TEXT="en general" ID="ID_903669612" CREATED="1551356995733" MODIFIED="1551402827022"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      # -*- coding: encoding -*-
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Windows-1252" ID="ID_744391648" CREATED="1551356995732" MODIFIED="1551402791563" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-14.926829962699236 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      # -*- coding: cp1252 -*-
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="UNIX shebang" ID="ID_594949161" CREATED="1551402835768" MODIFIED="1551402916628"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      #!/usr/bin/env python3
    </p>
    <p>
      # -*- coding: cp1252 -*-
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="Path" POSITION="right" ID="ID_90823870" CREATED="1551356995733" MODIFIED="1551404242409" HGAP_QUANTITY="56.146343424091945 pt" VSHIFT_QUANTITY="-61.46341749346744 pt">
<edge COLOR="#ff0000"/>
<node TEXT="OS" ID="ID_1201198400" CREATED="1551356995733" MODIFIED="1551404264886" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-15.804878784034486 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      import sys
    </p>
    <p>
      print sys.path
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="add to path" ID="ID_1615683347" CREATED="1551356995733" MODIFIED="1551404422409" HGAP_QUANTITY="17.512195285340997 pt" VSHIFT_QUANTITY="3.512195285340997 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      import sys
    </p>
    <p>
      print sys.path
    </p>
    <p>
      <b>['.', '/usr/local/bin',</b>&#160;<b>'/usr/local/lib/python2.6/dist-packages',...]</b>
    </p>
    <p>
      sys.path.append('/home/JoeBlow/python_scripts')
    </p>
    <p>
      print sys.path
    </p>
    <p>
      <b>['.', '/usr/local/bin', '/usr/local/lib/python2.6/dist-packages', '/home/JoeBlow/python_scripts',...]</b>
    </p>
    <p>
      &#160;&#160;
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="Compilation" POSITION="right" ID="ID_1259189249" CREATED="1551405066807" MODIFIED="1551405084993">
<edge COLOR="#7c0000"/>
<node TEXT="-O : remove asserts" ID="ID_203655298" CREATED="1551405099023" MODIFIED="1551405131456"/>
<node TEXT="-OO : also docstrings" ID="ID_1307292747" CREATED="1551405520201" MODIFIED="1551405546952"/>
<node TEXT="bytecode es&#xa;platform independent" ID="ID_709610557" CREATED="1551405838190" MODIFIED="1551405867393"/>
<node TEXT="mejora tiempo de carga" ID="ID_256860965" CREATED="1551405871737" MODIFIED="1551405882245"/>
<node TEXT="no afecta la ejecuci&#xf3;n" ID="ID_1645813202" CREATED="1551405883607" MODIFIED="1551405896237"/>
<node TEXT="recompilaci&#xf3;n autom&#xe1;tica" ID="ID_1131005649" CREATED="1551405907713" MODIFIED="1551406267726"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      *.pyc sin *.py =&gt; distribuci&#243;n independiente de la plataforma
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="Introspecci&#xf3;n" POSITION="left" ID="ID_25434398" CREATED="1551356995733" MODIFIED="1551406831344" HGAP_QUANTITY="73.70731985079694 pt" VSHIFT_QUANTITY="-37.75609931741573 pt">
<edge COLOR="#cc00ff"/>
<node TEXT="dir()" ID="ID_352937383" CREATED="1551356995733" MODIFIED="1551406991272" HGAP_QUANTITY="13.121951178664752 pt" VSHIFT_QUANTITY="-9.658537034687741 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      nombres definidos
    </p>
    <p>
      en el m&#243;dulo actual
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="dir(objeto)" ID="ID_966834988" CREATED="1551356995733" MODIFIED="1551406998150" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-7.902439392017243 pt"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      nombres definidos
    </p>
    <p>
      en el m&#243;dulo &quot;<b>objeto</b>&quot;
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="builtins" ID="ID_280310512" CREATED="1551406933252" MODIFIED="1551406976430"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      import builtins
    </p>
    <p>
      dir(builtins)
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="imports" POSITION="left" ID="ID_1822402846" CREATED="1551356995733" MODIFIED="1551407107940" HGAP_QUANTITY="84.24390570681993 pt" VSHIFT_QUANTITY="-36.00000167474521 pt">
<edge COLOR="#ff9900"/>
<node TEXT="modules" ID="ID_1274507163" CREATED="1551356995733" MODIFIED="1551407044542" HGAP_QUANTITY="14.0 pt" VSHIFT_QUANTITY="-21.07317171204598 pt"/>
<node TEXT="packages" ID="ID_1203076641" CREATED="1551356995733" MODIFIED="1551407050697"/>
</node>
<node TEXT="Idioms&#xa;(filosof&#xed;a)" POSITION="left" ID="ID_273436263" CREATED="1551356995732" MODIFIED="1551361199833" HGAP_QUANTITY="91.26829627750192 pt" VSHIFT_QUANTITY="-30.731708746733723 pt">
<edge COLOR="#007c7c"/>
<node TEXT="Zen of Python" ID="ID_588194298" CREATED="1551356995732" MODIFIED="1551360531924" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-12.292683498693489 pt">
<node TEXT="ZoP 1" ID="ID_542193342" CREATED="1551360556076" MODIFIED="1551404681134"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>The Zen of Python (2)</b>
    </p>
    <p>
      In the face of ambiguity, refuse the temptation to guess.
    </p>
    <p>
      There should be one&#8212;and preferably only one&#8212; obvious way to do it.
    </p>
    <p>
      Although that way may not be obvious at first unless you're Dutch.
    </p>
    <p>
      Now is better than never.
    </p>
    <p>
      Although never is often better than right now.
    </p>
    <p>
      If the implementation is hard to explain, it's a bad idea.
    </p>
    <p>
      If the implementation is easy to explain, it may be a good idea.
    </p>
    <p>
      Namespaces are one honking great idea&#8212;let's do more of those!
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="ZoP 2" ID="ID_841662801" CREATED="1551360567246" MODIFIED="1551360570039"/>
<node TEXT="import this" ID="ID_1854658447" CREATED="1551360622273" MODIFIED="1551360626548"/>
</node>
<node TEXT="IDEA 1.2" ID="ID_1104103844" CREATED="1551356995732" MODIFIED="1551356995732"/>
</node>
<node TEXT="IDEA 4" POSITION="left" ID="ID_575079876" CREATED="1551356995733" MODIFIED="1551356995733" HGAP_QUANTITY="67.56097810145019 pt" VSHIFT_QUANTITY="-42.14634342409195 pt">
<edge COLOR="#0000ff"/>
<node TEXT="IDEA 4.1" ID="ID_317662625" CREATED="1551356995734" MODIFIED="1551356995734" HGAP_QUANTITY="14.878048821335248 pt" VSHIFT_QUANTITY="-18.439025248040235 pt"/>
<node TEXT="IDEA 4.2" ID="ID_1522387997" CREATED="1551356995734" MODIFIED="1551356995734"/>
</node>
</node>
</map>
