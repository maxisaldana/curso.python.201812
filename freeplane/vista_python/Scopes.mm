<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Scopes" FOLDED="false" ID="ID_191153586" CREATED="1552074222407" MODIFIED="1552074261227" ICON_SIZE="36.0 pt" LINK="menuitem:_ExternalImageAddAction" STYLE="oval">
<font SIZE="22"/>
<hook NAME="MapStyle">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" ICON_SIZE="64.0 pt" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font NAME="Segoe Print" SIZE="22"/>
<edge COLOR="#ffffff"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" ICON_SIZE="32.0 px" COLOR="#000000" BACKGROUND_COLOR="#ffffcc" STYLE="bubble" SHAPE_HORIZONTAL_MARGIN="2.6 pt" SHAPE_VERTICAL_MARGIN="2.6 pt" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="18" BOLD="false" ITALIC="true"/>
<edge STYLE="sharp_bezier" WIDTH="8"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" ICON_SIZE="28.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="16"/>
<edge STYLE="bezier" WIDTH="4"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" ICON_SIZE="24.0 px" COLOR="#000000" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="14"/>
<edge STYLE="bezier" WIDTH="3"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" ICON_SIZE="24.0 px" COLOR="#111111" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="13"/>
<edge STYLE="bezier" WIDTH="2"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5" ICON_SIZE="24.0 px" BORDER_WIDTH_LIKE_EDGE="true">
<font SIZE="12"/>
<edge STYLE="bezier" WIDTH="1"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6" ICON_SIZE="24.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7" ICON_SIZE="16.0 px">
<font SIZE="10"/>
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11" ICON_SIZE="16.0 px">
<edge STYLE="bezier"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="15" RULE="ON_BRANCH_CREATION"/>
<hook NAME="accessories/plugins/AutomaticLayout.properties" VALUE="ALL"/>
<edge COLOR="#ffffff"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      1. Write the central idea
    </p>
    <p>
      2. Save the map
    </p>
    <p>
      3. Select central node,
    </p>
    <p>
      &#160;&#160;&#160;&#160;click on icon to add picture
    </p>
    <p>
      4. Delete menu links Add image..
    </p>
    <p>
      &#160;&#160;&#160;&#160;with CTRL+K and delete field with link
    </p>
    <p>
      5. Delete node details with right click on mouse
    </p>
    <p>
      &#160;&#160;&#160;&#160;on central node - Remove node details
    </p>
  </body>
</html>
</richcontent>
<node TEXT="IDEA 1" POSITION="right" ID="ID_273436263" CREATED="1552074222419" MODIFIED="1552074222419" HGAP_QUANTITY="91.26829627750192 pt" VSHIFT_QUANTITY="-30.731708746733723 pt">
<edge COLOR="#007c7c"/>
<node TEXT="IDEA 1.1" ID="ID_588194298" CREATED="1552074222419" MODIFIED="1552074222419" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-12.292683498693489 pt"/>
<node TEXT="IDEA 1.2" ID="ID_1104103844" CREATED="1552074222420" MODIFIED="1552074222420"/>
</node>
<node TEXT="IDEA 2" POSITION="right" ID="ID_495676614" CREATED="1552074222420" MODIFIED="1552074222420" HGAP_QUANTITY="101.80488213352491 pt" VSHIFT_QUANTITY="-42.14634342409195 pt">
<edge COLOR="#009900"/>
<node TEXT="IDEA 2.1" ID="ID_744391648" CREATED="1552074222420" MODIFIED="1552074222420" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-14.926829962699236 pt"/>
<node TEXT="IDEA 2.2" ID="ID_903669612" CREATED="1552074222420" MODIFIED="1552074222420"/>
</node>
<node TEXT="Global,&#xa; NonLocal,&#xa; Local" POSITION="right" ID="ID_90823870" CREATED="1552074222421" MODIFIED="1552074323859" HGAP_QUANTITY="56.146343424091945 pt" VSHIFT_QUANTITY="-61.46341749346744 pt">
<edge COLOR="#ff0000"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      If you put some name into module's namespace; it is visible in any module that uses module i.e., it is global for the whole Python process.
    </p>
    <p>
      
    </p>
    <p>
      In general, your application should use as few mutable globals as possible. See Why globals are bad?:
    </p>
    <p>
      
    </p>
    <p>
      Non-locality
    </p>
    <p>
      No Access Control or Constraint Checking
    </p>
    <p>
      Implicit coupling
    </p>
    <p>
      Concurrency issues
    </p>
    <p>
      Namespace pollution
    </p>
    <p>
      Testing and Confinement
    </p>
    <p>
      Therefore It would be bad if nonlocal allowed to create globals by accident. If you want to modify a global variable; you could use global keyword directly.
    </p>
    <p>
      
    </p>
    <p>
      global is the most destructive: may affect all uses of the module anywhere in the program
    </p>
    <p>
      nonlocal is less destructive: limited by the outer() function scope (the binding is checked at compile time)
    </p>
    <p>
      no declaration (local variable) is the least destructive option: limited by inner() function scope
    </p>
    <p>
      You can read about history and motivation behind nonlocal in PEP: 3104 Access to Names in Outer Scopes.
    </p>
  </body>
</html>

</richcontent>
<node TEXT="Global" ID="ID_1201198400" CREATED="1552074222421" MODIFIED="1552074749736" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-15.804878784034486 pt">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_998437998" STARTINCLINATION="567;0;" ENDINCLINATION="567;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Module (.py file)
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="NonLocal (enclosed)" ID="ID_1615683347" CREATED="1552074222421" MODIFIED="1552074749739" HGAP_QUANTITY="17.512195285340997 pt" VSHIFT_QUANTITY="3.512195285340997 pt">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_998437998" STARTINCLINATION="397;0;" ENDINCLINATION="397;0;" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Definidos en toda otra funci&#243;n
    </p>
    <p>
      &#160;que engloba a la ff actual
    </p>
  </body>
</html>

</richcontent>
</node>
<node TEXT="Global vs NonLocal" ID="ID_567369711" CREATED="1552074761323" MODIFIED="1552074770136">
<node TEXT="Ej 1" ID="ID_998437998" CREATED="1552074637988" MODIFIED="1552074989980" LINK="https://stackoverflow.com/questions/16873615/why-doesnt-pythons-nonlocal-keyword-like-the-global-scope"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      &gt;&gt;&gt;def tester(start):
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;def nested(label):
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;nonlocal state&#160;&#160;&#160;#nonlocals must already exist in enclosing def!
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;state = 0
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;print(label, state)
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;return nested
    </p>
    <p>
      SyntaxError: no binding for nonlocal 'state' found
    </p>
    <p>
      
    </p>
    <p>
      &gt;&gt;&gt;def tester(start):
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;def nested(label):
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;global state&#160;&#160;&#160;#Globals dont have to exits yet when declared
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;state = 0&#160;&#160;&#160;&#160;&#160;&#160;#This creates the name in the module now
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;print(label, state)
    </p>
    <p>
      &#160;&#160;&#160;&#160;&#160;&#160;return nested
    </p>
    <p>
      
    </p>
    <p>
      &gt;&gt;&gt; F = tester(0)
    </p>
    <p>
      &gt;&gt;&gt; F('abc')
    </p>
    <p>
      abc 0
    </p>
    <p>
      &gt;&gt;&gt; state
    </p>
    <p>
      0
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="Local" ID="ID_340585331" CREATED="1552074468938" MODIFIED="1552074480925"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      Definida en la ff actual
    </p>
  </body>
</html>

</richcontent>
</node>
</node>
<node TEXT="IDEA 6" POSITION="left" ID="ID_25434398" CREATED="1552074222421" MODIFIED="1552074222421" HGAP_QUANTITY="73.70731985079694 pt" VSHIFT_QUANTITY="-37.75609931741573 pt">
<edge COLOR="#cc00ff"/>
<node TEXT="IDEA 6.1" ID="ID_352937383" CREATED="1552074222421" MODIFIED="1552074222421" HGAP_QUANTITY="13.121951178664752 pt" VSHIFT_QUANTITY="-9.658537034687741 pt"/>
<node TEXT="IDEA 6.2" ID="ID_966834988" CREATED="1552074222421" MODIFIED="1552074222421" HGAP_QUANTITY="15.756097642670499 pt" VSHIFT_QUANTITY="-7.902439392017243 pt"/>
</node>
<node TEXT="IDEA 5" POSITION="left" ID="ID_1822402846" CREATED="1552074222422" MODIFIED="1552074222422" HGAP_QUANTITY="84.24390570681993 pt" VSHIFT_QUANTITY="-36.00000167474521 pt">
<edge COLOR="#ff9900"/>
<node TEXT="IDEA 5.1" ID="ID_1274507163" CREATED="1552074222422" MODIFIED="1552074222422" HGAP_QUANTITY="14.0 pt" VSHIFT_QUANTITY="-21.07317171204598 pt"/>
<node TEXT="IDEA 5.2" ID="ID_1203076641" CREATED="1552074222422" MODIFIED="1552074222422"/>
</node>
<node TEXT="IDEA 4" POSITION="left" ID="ID_575079876" CREATED="1552074222422" MODIFIED="1552074222422" HGAP_QUANTITY="67.56097810145019 pt" VSHIFT_QUANTITY="-42.14634342409195 pt">
<edge COLOR="#0000ff"/>
<node TEXT="IDEA 4.1" ID="ID_317662625" CREATED="1552074222422" MODIFIED="1552074222422" HGAP_QUANTITY="14.878048821335248 pt" VSHIFT_QUANTITY="-18.439025248040235 pt"/>
<node TEXT="IDEA 4.2" ID="ID_1522387997" CREATED="1552074222423" MODIFIED="1552074222423"/>
</node>
</node>
</map>
