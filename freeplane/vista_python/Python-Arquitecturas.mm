<map version="freeplane 1.7.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Python&#xa;(arquitecturas)" FOLDED="false" ID="ID_1723255651" CREATED="1283093380553" MODIFIED="1553808945188"><hook NAME="MapStyle" zoom="1.282">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#808080ff,#808080ff,#808080ff,#808080ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600.0 px" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="5" RULE="ON_BRANCH_CREATION"/>
<node TEXT="Arquitecturas (hardware)" FOLDED="true" POSITION="right" ID="ID_1450198395" CREATED="1545145905426" MODIFIED="1551538365883" LINK="https://www.python.org/download/other/">
<edge COLOR="#00ff00"/>
<richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      platform module
    </p>
    <p>
      * access underlying platform's data
    </p>
    <p>
      &#160;* hardware
    </p>
    <p>
      &#160;* operating system
    </p>
    <p>
      &#160;* interpreter version information
    </p>
  </body>
</html>
</richcontent>
<node TEXT="x86 32/64 variants" ID="ID_1368623424" CREATED="1545145913978" MODIFIED="1545314930216"/>
<node TEXT="PPC" ID="ID_743560347" CREATED="1551363642024" MODIFIED="1551363643936"/>
<node TEXT="ARM, iOS" ID="ID_327148902" CREATED="1545146102495" MODIFIED="1551450739386"/>
<node TEXT="...otras... (PPC, Symbian, ...)" ID="ID_127236832" CREATED="1545146109869" MODIFIED="1551450779572"/>
<node TEXT="AIX, IBMi (AS/400, iSeries, System i)" ID="ID_1324032671" CREATED="1551450794933" MODIFIED="1551450824874"/>
<node TEXT="Solaris, OS/390, z/OS, VMS, HP-UX" ID="ID_268473660" CREATED="1551450802521" MODIFIED="1551450836061"/>
<node TEXT="Platform dep: PI/PD" ID="ID_1352061724" CREATED="1545315015918" MODIFIED="1551363949569"><richcontent TYPE="DETAILS">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b>Platform independency</b>: vague term
    </p>
    <p>
      
    </p>
    <p>
      <b>* Platform Independent language:</b>
    </p>
    <p>
      &#160;&#160;Scripts (maybe) and many modules
    </p>
    <p>
      
    </p>
    <p>
      <b>* Platform dependent language:</b>
    </p>
    <p>
      &#160;&#160;Scripts (by design) and platform specific modules
    </p>
    <p>
      
    </p>
    <p>
      <b>Idea of not dependency:</b>
    </p>
    <p>
      
    </p>
    <p>
      <b>&#160;* compileable on any platform with the same source&#160;code:</b>
    </p>
    <p>
      &#160;&#160;&#160;&#160;Almost any language
    </p>
    <p>
      
    </p>
    <p>
      <b>* Copy on a different platform and run immedatly:</b>
    </p>
    <p>
      &#160;&#160;&#160;&#160;Any Scripting Language and Java and .NET for platforms which have the framework (JRE and .NET/mono).
    </p>
  </body>
</html>
</richcontent>
<hook NAME="AlwaysUnfoldedNode"/>
</node>
</node>
</node>
</map>
